package com.example.app_test;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.example.app_test.adapter.FoodAdapter;
import com.example.app_test.adapter.FoodCursorAdapter;
import com.example.app_test.db.DBHelper;
import com.example.app_test.models.Food;
import com.example.app_test.modify.FoodModify;

import java.util.ArrayList;
import java.util.List;

public class DBFoodActivity2 extends AppCompatActivity {
    ListView listView;
    FoodCursorAdapter adapter;
    Cursor currentCursor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DBHelper.getInstance(this);
        setContentView(R.layout.activity_dbfood);
        setContentView(R.layout.activity_subject);
        listView = findViewById(R.id.as_listview);
        currentCursor = FoodModify.getFoodCursor();
        adapter =  new FoodCursorAdapter(this,currentCursor);
        listView.setAdapter(adapter);
      registerForContextMenu(listView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater =  getMenuInflater();
        inflater.inflate(R.menu.food_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.food_menu_context,menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected( MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int postion = info.position;
        switch (item.getItemId()){
            case R.id.menu_fmc_edit_food:
                showSubjectDialog(postion);
                break;
            case R.id.menu_fmc_remove_food:
                  currentCursor.moveToPosition(postion);
                    int id = currentCursor.getInt(currentCursor.getColumnIndex("_id"));
                    FoodModify.delete(id);
                  updateFood();
                break;

        }
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_fm_add_food:
                showSubjectDialog(-1 );
                break;
            case R.id.menu_fm_exit:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    void updateFood( ){
        currentCursor = FoodModify.getFoodCursor();
        adapter.changeCursor(currentCursor);
        adapter.notifyDataSetChanged();
    }
    private void showSubjectDialog( final  int postion) {
        View v = getLayoutInflater().inflate(R.layout.dialog_food_editor,null);
        EditText titletxt = v.findViewById(R.id.dfe_title);
        EditText destxt = v.findViewById(R.id.dfe_description);
        EditText imgtxt = v.findViewById(R.id.dfe_thumbnai);
        if(postion >= 0){
            currentCursor.moveToPosition(postion);
            String title =currentCursor.getString(currentCursor.getColumnIndex("title"));
            String des = currentCursor.getString(currentCursor.getColumnIndex("description"));
            String thumbnail  = currentCursor.getString(currentCursor.getColumnIndex("thumbnail"));

            titletxt.setText(title);
            destxt.setText(des);
            imgtxt.setText(thumbnail);
        }else {
            imgtxt.setText("https://dulichviet247.com/wp-content/uploads/2017/11/anhr-cafe-sapa.jpg");
        }
        AlertDialog.Builder builder =  new AlertDialog.Builder(this);
        builder.setView(v);
        builder.setTitle("Food Editor")

                .setPositiveButton("save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String title = titletxt.getText().toString();
                        String des = destxt.getText().toString();
                        String thumbnai = imgtxt.getText().toString();

                        if(postion >= 0){
                            currentCursor.moveToPosition(postion);
                            int id = currentCursor.getInt(currentCursor.getColumnIndex("_id"));
                            Food food = new Food(id,thumbnai,title,des);
                            FoodModify.update(food  );
                        }else {
                            Food food = new Food(thumbnai,title,des);
                            FoodModify.insert(food);

                        }
                      updateFood();

                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).show();
    }

}