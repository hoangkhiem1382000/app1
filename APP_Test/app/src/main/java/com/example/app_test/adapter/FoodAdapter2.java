package com.example.app_test.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app_test.R;
import com.example.app_test.models.Food;

import java.util.List;

public class FoodAdapter2 extends RecyclerView.Adapter<FoodAdapter2.FoodHolder> {
    Activity activity ;
    List<Food>DataList ;

    public  FoodAdapter2(Activity activity , List<Food> DataList){
       this.activity = activity;
       this.DataList = DataList;
    }
    @Override
    public FoodHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        View view = activity.getLayoutInflater().inflate(R.layout.food_item,null);
        TextView titleView = view.findViewById(R.id.fi_title);
        TextView desView = view.findViewById(R.id.fi_description);

        FoodHolder holder= new FoodHolder(view,titleView,desView);
        Log.d(FoodAdapter2.class.getName(),"Count(1)"+ ++count );
        return holder;
    }
    static int count = 0;
    static int count1 = 0;
    @Override
    public void onBindViewHolder( FoodAdapter2.FoodHolder holder, int position) {
       Food food= DataList.get(position);
       holder.titleView.setText(food.getTitle());
       holder.desView.setText(food.getDescription());
       holder.postion = position;
       Log.d(FoodAdapter2.class.getName(),"Count(2)"+ ++count1 );
    }
    @Override
    public int getItemCount() {
        return DataList.size();
    }
        class FoodHolder extends RecyclerView.ViewHolder implements  View.OnCreateContextMenuListener {
        TextView titleView,desView;
        int postion = -1;

        public FoodHolder(View itemView) {
                super(itemView);
            }
        public FoodHolder( View itemView, TextView titleView, TextView desView) {
            super(itemView);
            this.titleView = titleView;
            this.desView = desView;
            itemView.setOnCreateContextMenuListener(this);
        }
        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.add(Menu.NONE,R.id.menu_smc_edit_subject,postion,"Edit Food");
            menu.add(Menu.NONE,R.id.menu_smc_remove_subject,postion,"Remove Food");
        }
    }
}
