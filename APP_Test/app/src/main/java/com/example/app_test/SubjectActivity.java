package com.example.app_test;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class SubjectActivity extends AppCompatActivity {
    ListView listView;
    List<String> dataList = new ArrayList<>();
    ArrayAdapter<String>adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject);
        listView = findViewById(R.id.as_listview);
        dataList.add("Lap trinh C++");
        dataList.add("HTML/CSS/JS");
        dataList.add("Bootstrap/JQuery");
        adapter = new ArrayAdapter<String>(this, R.layout.subject_item, R.id.si_textview,dataList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                 String v = dataList.get(position);
                Toast.makeText(SubjectActivity.this, v, Toast.LENGTH_SHORT).show();
            }
        });
    registerForContextMenu(listView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater =  getMenuInflater();
        inflater.inflate(R.menu.subject_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.subject_menu_context,menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected( MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int postion = info.position;
        switch (item.getItemId()){
            case R.id.menu_smc_edit_subject:
                showSubjectDialog(postion,dataList.get(postion));
                break;
            case R.id.menu_smc_remove_subject:
                dataList.remove(postion);
                adapter.notifyDataSetChanged();
                break;

        }
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_sm_add_subject:
                showSubjectDialog(-1 , null);
                break;
            case R.id.menu_sm_exit:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void showSubjectDialog( final  int postion,String subjectname) {
        View v = getLayoutInflater().inflate(R.layout.dialog_subject_editor,null);
        EditText subjectNameTxt = v.findViewById(R.id.dse_subject_name);
    if(subjectname != null){
        subjectNameTxt.setText(subjectname);
    }
        AlertDialog.Builder builder =  new AlertDialog.Builder(this);
        builder.setView(v);
        builder.setTitle("Subject Editor")

                .setPositiveButton("save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String subjectname = subjectNameTxt.getText().toString();
                        if(postion >= 0){
                            dataList.set(postion,subjectname);
                        }else {
                            dataList.add(subjectname);
                        }

                        adapter.notifyDataSetChanged();

                     }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).show();


    }
}