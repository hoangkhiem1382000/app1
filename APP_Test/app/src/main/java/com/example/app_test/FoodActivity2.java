package com.example.app_test;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;

import com.example.app_test.adapter.FoodAdapter2;
import com.example.app_test.models.Food;

import java.util.ArrayList;
import java.util.List;

public class FoodActivity2 extends AppCompatActivity {
            RecyclerView recyclerView;
            List<Food> dataList = new ArrayList<>();
            FoodAdapter2 adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food2);
        recyclerView = findViewById(R.id.af2_listview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        for (int i = 0 ; i <= 3 ; i ++){
            dataList.add(new Food("thumbnai 1","title"+i,"Noi dung "+2));
        }
        adapter = new FoodAdapter2(this,dataList);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater =  getMenuInflater();
        inflater.inflate(R.menu.subject_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_sm_add_subject:
                showSubjectDialog(-1 );
                break;
            case R.id.menu_sm_exit:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onContextItemSelected( MenuItem item) {
        int postion = -1;
        try {
            postion =   item.getOrder();
        }
        catch (Exception e){
            return  super.onContextItemSelected(item);
        }
        switch (item.getItemId()){
            case R.id.menu_smc_edit_subject:
                showSubjectDialog(postion);
                break;
            case R.id.menu_smc_remove_subject:
                dataList.remove(postion);
                adapter.notifyDataSetChanged();
                break;

        }
        return super.onContextItemSelected(item);
    }

    private void showSubjectDialog( final  int postion) {
        View v = getLayoutInflater().inflate(R.layout.dialog_food_editor,null);
        EditText titletxt = v.findViewById(R.id.dfe_title);
        EditText destxt = v.findViewById(R.id.dfe_description);
        if(postion>=0){
            titletxt.setText(dataList.get(postion).getTitle());
            destxt.setText(dataList.get(postion).getDescription());
        }
        AlertDialog.Builder builder =  new AlertDialog.Builder(this);
        builder.setView(v);
        builder.setTitle("Food Editor")

                .setPositiveButton("save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String title = titletxt.getText().toString();
                        String des = destxt.getText().toString();
                        Food food= new Food(title,des);

                        if(postion >= 0){
                            dataList.set(postion,food);
                        }else {
                            dataList.add(food);
                        }

                        adapter.notifyDataSetChanged();

                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).show();


    }

}