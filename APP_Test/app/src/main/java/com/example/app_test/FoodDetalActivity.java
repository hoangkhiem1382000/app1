package com.example.app_test;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.app_test.models.Food;
import com.squareup.picasso.Picasso;

public class FoodDetalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_detal);
        //Láy du lieu truyen sang
        Food food = getIntent().getParcelableExtra("thisFood");
        String thumbnail = food.getThumbnail();
        String title = food.getTitle();
        String des = food.getDescription();

//        String thumbnail = getIntent().getStringExtra("thumbnail");
//        String title = getIntent().getStringExtra("title");
//        String des = getIntent().getStringExtra("description");
        ImageView thumbnailImg = findViewById(R.id.afd_thumnail);
        TextView titleView = findViewById(R.id.afd_title);
        TextView desView = findViewById(R.id.afd_des);
        Picasso.with(this).load(thumbnail).into(thumbnailImg);
        titleView.setText(title);
        desView.setText(des);
    }
}