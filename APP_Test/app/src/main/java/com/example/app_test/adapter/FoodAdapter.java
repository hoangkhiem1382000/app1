package com.example.app_test.adapter;

import android.app.Activity;
import android.media.Image;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.app_test.R;
import com.example.app_test.models.Food;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FoodAdapter extends BaseAdapter {
    Activity activity;
    List<Food> dataList;
    public FoodAdapter(Activity activity, List<Food> dataList){
    this.activity = activity;
    this.dataList = dataList;
    }
    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int i) {
        return dataList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    int count = 0;
    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        FoodHolder foodHolder = null;
        if(view == null){
            Log.d(FoodAdapter.class.getName(),"C => " + ++count);
            view = activity.getLayoutInflater().inflate(R.layout.food_item,null);
            TextView titleView =view.findViewById(R.id.fi_title);
            TextView desView = view.findViewById(R.id.fi_description);
            ImageView thumbnaiImg = view.findViewById(R.id.fi_thumbnail);

             foodHolder = new FoodHolder(titleView,desView,thumbnaiImg);
            view.setTag(foodHolder);
        }else {
            foodHolder = (FoodHolder)view.getTag();

        }

        Food food = dataList.get(position);
        foodHolder.titleView.setText(food.getTitle());
        foodHolder.desView.setText(food.getDescription());
        Picasso.with(activity).load(food.getThumbnail()).into(foodHolder.thumbnaiImg);
        return view;
    }

    public List<Food> getDataList() {
        return dataList;
    }

    public void setDataList(List<Food> dataList) {
        this.dataList = dataList;
    }

    class FoodHolder{
                TextView titleView;
                TextView desView;
                ImageView thumbnaiImg;
                public FoodHolder(TextView titleView, TextView desView , ImageView thumbnaiImg){
                    this.titleView = titleView;
                    this.desView = desView;
                    this.thumbnaiImg = thumbnaiImg;
                }
    }
}
