package com.example.app_test.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.util.Log;

import com.example.app_test.modify.FoodModify;

public class DBHelper extends SQLiteOpenHelper {
        static final  String DB_NAME = "FoodApp";
        static final    int VERSION = 1;
        static  DBHelper instance = null;
    public DBHelper(Context context) {
        super(context, DB_NAME,null, VERSION);
    }
    public synchronized static DBHelper getInstance(Context context){
        if (instance == null){
            instance = new DBHelper(context);
        }
        return instance;

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
//ham nay chi goi một lần duy nhất khi data chưa khởi tao
        db.execSQL(FoodModify.TABLE_SQL);
        Log.d(DBHelper.class.getName(),"Create food table is success");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//chỉ gọi một lần khi có sự thay đổi của db
    }
}
