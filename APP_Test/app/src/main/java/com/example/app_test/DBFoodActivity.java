package com.example.app_test;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.example.app_test.adapter.FoodAdapter;
import com.example.app_test.db.DBHelper;
import com.example.app_test.models.Food;
import com.example.app_test.modify.FoodModify;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class DBFoodActivity extends AppCompatActivity {
    ListView listView;
    List<Food> dataList = new ArrayList<>();
    FoodAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DBHelper.getInstance(this);
       setContentView(R.layout.activity_dbfood);
        setContentView(R.layout.activity_subject);
        listView = findViewById(R.id.as_listview);
        dataList = FoodModify.getFoodList();
        adapter = new FoodAdapter(this,dataList);
        listView.setAdapter(adapter);
        registerForContextMenu(listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Food food = dataList.get(position);
                Intent i = new Intent(DBFoodActivity.this,FoodDetalActivity.class);
                i.putExtra("thisFood",food);
//                i.putExtra("thumbnail",food.getThumbnail());
//                i.putExtra("title",food.getTitle());
//                i.putExtra("description",food.getDescription());
                startActivity(i);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater =  getMenuInflater();
        inflater.inflate(R.menu.food_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.food_menu_context,menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected( MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int postion = info.position;
        switch (item.getItemId()){
            case R.id.menu_fmc_edit_food:
                showSubjectDialog(postion);
                break;
            case R.id.menu_fmc_remove_food:
                dataList.remove(postion);
                FoodModify.delete(dataList.get(postion).get_id());
                dataList = FoodModify.getFoodList();
                adapter.setDataList(dataList);
                adapter.notifyDataSetChanged();
                break;

        }
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_fm_add_food:
                showSubjectDialog(-1 );
                break;
            case R.id.menu_fm_exit:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void showSubjectDialog( final  int postion) {
        View v = getLayoutInflater().inflate(R.layout.dialog_food_editor,null);
        EditText titletxt = v.findViewById(R.id.dfe_title);
        EditText destxt = v.findViewById(R.id.dfe_description);
        EditText imgtxt = v.findViewById(R.id.dfe_thumbnai);
        if(postion >= 0){
            titletxt.setText(dataList.get(postion).getTitle());

            destxt.setText(dataList.get(postion).getDescription());

            imgtxt.setText(dataList.get(postion).getThumbnail());
        }else {
            imgtxt.setText("https://dulichviet247.com/wp-content/uploads/2017/11/anhr-cafe-sapa.jpg");
        }
        AlertDialog.Builder builder =  new AlertDialog.Builder(this);
        builder.setView(v);
        builder.setTitle("Food Editor")

                .setPositiveButton("save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String title = titletxt.getText().toString();
                        String des = destxt.getText().toString();
                        String thumbnai = imgtxt.getText().toString();

                        if(postion >= 0){
                            dataList.get(postion).setTitle(title);
                            dataList.get(postion).setDescription(des);
                            dataList.get(postion).setThumbnail(thumbnai);
                            FoodModify.update(dataList.get(postion));
                        }else {
                            Food food = new Food(thumbnai,title,des);
                            dataList.add(food);
                            FoodModify.insert(food);
                            dataList = FoodModify.getFoodList();
                            adapter.setDataList(dataList);
                        }
                        adapter.notifyDataSetChanged();

                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).show();
    }



}